import getpass, imaplib
from datetime import datetime
import smtplib
import email
import os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import glob
import shutil

emailUsername = 'example@gmail.com'
emailPassword = 'password'
installDir = 'C:\Users\Jayden\Documents\wip2018'

_=os.system('title Server')

def mail_send(subject,content,emailaddr):
  msg = MIMEMultipart()
  msg['From'] = emailUsername
  msg['To'] = emailaddr
  msg['Subject'] = subject
  body = content
  msg.attach(MIMEText(body, 'plain'))
  server = smtplib.SMTP('smtp.gmail.com', 587)
  server.starttls()
  server.login(emailUsername, emailPassword)
  text = msg.as_string()
  server.sendmail(emailUsername, emailaddr, text)
  server.quit()

def mail_delete():
    m = imaplib.IMAP4_SSL('imap.gmail.com', 993)
    m.login(emailUsername,emailPassword)
    m.list()
    m.select('INBOX')
    typ, data = m.search(None, '(FROM "bushtrackernz@gmail.com")')
    ids = data[0]
    ids_list = ids.split()
    for msg in ids_list:
        m.store(msg, '+FLAGS', '\\Deleted')
    m.expunge()
    m.close()
    m.logout()
def mail_read():
    M = imaplib.IMAP4_SSL("imap.gmail.com", 993)
    M.login(emailUsername, emailPassword)
    M.select('inbox')
    typ, data = M.search(None, 'ALL')
    mail_ids = data[0]
    id_list = mail_ids.split()   
    first_email_id = int(id_list[0])
    latest_email_id = int(id_list[-1])
    M.fetch(latest_email_id, '(RFC822)' )
    typ, data = M.fetch(latest_email_id, '(RFC822)' )
    _=os.system('cls')
    for response_part in data:
        if isinstance(response_part, tuple):
            msg = email.message_from_string(response_part[1])
            email_subject = msg['subject']
            email_from = msg['from']
            #print 'From : ' + email_from
            print 'Subject : ' + email_subject
            return email_subject



_=os.chdir(installDir)
_=os.chdir('away')
while True:
    _=os.chdir(installDir)
    _=os.chdir('away')
    file_names = (glob.glob('*.txt'))
    num = int('0')
    peopleAway = True
    if not file_names:
        print('Nobody Away')
        peopleAway = False
    if peopleAway == True:
        for i in file_names:
            f= open(file_names[num],'r')
            return_name = f.readline()
            return_name_last = f.readline()
            return_mail = f.readline()
            return_date = f.readline()
            return_time = f.readline()
            f.close()

            return_date = return_date.rstrip('\n')
            return_date_whole = return_date
            return_date = return_date.split('-')
            return_time = return_time.rstrip('\n')
            return_time_whole = return_time
            return_time = return_time.split(':')
            return_name = return_name.rstrip('\n')
            return_name_last = return_name_last.rstrip('\n')
            return_mail = return_mail.rstrip('\n')
            return_date_whole = return_date_whole.rstrip('\n')
            return_time_whole = return_time_whole.rstrip('\n')
        
            return_date_year = int(return_date[2])
            return_date_month = int(return_date[1])
            return_date_day = int(return_date[0])
            return_time_hour = int(return_time[0])
            return_time_min = int(return_time[1])

            return_time = datetime(return_date_year, return_date_month, return_date_day, return_time_hour, return_time_min)
            if datetime.now() > return_time:
                print("hh")
                print file_names[num]
                usr_miss_subj = return_name + " " + return_name_last + " has not returned from their trip yet"
                usr_miss_cont = "They expected to return by " + return_date_whole + return_time_whole + ",please try and make contact with them or alert authoritys"
                mail_send(usr_miss_subj,usr_miss_cont,return_mail)
                shutil.move(file_names[num], 'C:\\Users\\Jayden\\Documents\\wip2018\\overdue\\' + file_names[num])
            num = int('1')

    newMail = True
    try:
        station_input = mail_read()
    except Exception:
        _=os.system('cls')
        print('no new email')
        newMail = False
    
    if  newMail == True:
        station_input = station_input.rstrip('\n')
        station_input_spl = station_input.split('#')
        _=os.chdir(installDir)
        _=os.chdir('progress')
        if os.path.isfile(station_input_spl[0] + '#' + station_input_spl[1] + '.txt') == True:
            f= open(station_input_spl[0] + '#' + station_input_spl[1] + '.txt','a+')
            f.write(station_input_spl[2])
            f.write("\n")
            f.write(str(datetime.now()))
            f.write("\n")
            f.close()
        if os.path.isfile(station_input_spl[0] + '#' + station_input_spl[1] + '.txt') == False:
            f= open(station_input_spl[0] + '#' + station_input_spl[1] + '.txt','w+')
            f.write(station_input_spl[2])
            f.write("\n")
            f.write(str(datetime.now()))
            f.write("\n")
            f.close()
        if station_input_spl[3] == 'sos':
            f= open(station_input_spl[0] + '#' + station_input_spl[1] + '.txt','a+')
            f.write(station_input_spl[3])
            f.write("\n")
            f.write(str(datetime.now()))
            f.write("\n")
            f.close()
            _=os.chdir(installDir)
            _=os.chdir('away')
            f= open(station_input_spl[0] + '#' + station_input_spl[1] + '.txt','r')
            f.readline()
            f.readline()
            sosEmail = f.readline()
            sosEmail = sosEmail.rstrip('\n')

            usr_sos_subj = station_input_spl[0] + " " + station_input_spl[1] + " has raised an SOS"
            usr_sos_cont = "They are at station " + station_input_spl[2] + ", please try and contact them or alert authorities"
            mail_send(usr_sos_subj,usr_sos_cont,sosEmail)
            print('sos')
        mail_delete()
    #except Exception:
        #_=os.system('cls')
        #print('no incoming mail')
