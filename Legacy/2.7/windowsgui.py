import getpass, imaplib
import time
from datetime import datetime
import smtplib
import email
import os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

emailUsername = 'xxx@gmail.com'
emailPassword = 'xxx'
installDir = 'C:\Users\Jayden\Documents\wip2018'
ownerMail = 'xxx@outlook.com'

_=os.system('title UserGUI')

def mail_send(subject,content,emailaddr):
  msg = MIMEMultipart()
  msg['From'] = emailUsername
  msg['To'] = emailaddr
  msg['Subject'] = subject
  body = content
  msg.attach(MIMEText(body, 'plain'))
  server = smtplib.SMTP('smtp.gmail.com', 587)
  server.starttls()
  server.login(emailUsername, emailPassword)
  text = msg.as_string()
  server.sendmail(emailUsername, emailaddr, text)
  server.quit()

usr_choice = "a"
usr_choice_1 = "a"

#M = imaplib.IMAP4_SSL("imap.gmail.com", 993)
#M.login(emailUsername, emailPassword)
#M.select('inbox')
#typ, data = M.search(None, 'ALL')
#mail_ids = data[0]
#id_list = mail_ids.split()   
#first_email_id = int(id_list[0])
#latest_email_id = int(id_list[-1])
#M.fetch(latest_email_id, '(RFC822)' )

#typ, data = M.fetch(latest_email_id, '(RFC822)' )
#for response_part in data:
#    if isinstance(response_part, tuple):
#        msg = email.message_from_string(response_part[1])
#        email_subject = msg['subject']
#        email_from = msg['from']
#        print 'From : ' + email_from
#        print 'Subject : ' + email_subject

while True:
    while usr_choice == "a":
        _=os.system("cls")
        print('JBGuy Tech 2018 All Rights Reserved')
        print('Welcome to the NZ Bush Tracker, The date/time is ' + str(datetime.now()))
        print('Options:')
        print('1.Start Trail')
        print('2.Check Out')
        usr_choice = raw_input('Option:')
        if usr_choice != '1' and usr_choice != '2':
            usr_choice = "a"
        time.sleep(0.5)


    if usr_choice == "1":
        usr_choice_1 = "a"
        while usr_choice_1 == "a":
            _=os.system("cls")
            print("Please Choose Check In Option")
            print('1.Temporary Account')
            print('2.Sign into existing account')
            print('3.Create New Account')
            print('4.Back')
            usr_choice_1 = raw_input('Option:')
            if usr_choice_1 != '1' and usr_choice_1 != '2' and usr_choice_1 != '3' and usr_choice_1 != '4':
                usr_choice_1 = "a"
            elif usr_choice_1 != 'a':
                break

    if usr_choice == "2":
        usr_choice_2 = 'a'
        while usr_choice_2 == 'a':
            _=os.system("cls")
            print('Please Scan Barcode')
            barin = raw_input('Here:')
            if "#" not in barin:
                _=os.system("cls")
                print('Invalid Barcode')
                print('Please Scan A Valid Barcode')
                time.sleep(2.5)
                break
            if barin == '':
                break
            _=os.chdir(installDir)
            _=os.chdir('away')
            barin_path = barin + '.txt'
            
            f= open(barin_path,"r")
            usrturn_name_first = f.readline()
            usrturn_name_last = f.readline()
            usrturn_mail_1 = f.readline()
            f.close()
            usrturn_mail_1 = usrturn_mail_1.rstrip('\n')
            
            if os.path.isfile(barin_path) == False:
              _=os.system("cls")
              print("User Not On Trail")
              time.sleep(2.5)
            else:
              os.remove(barin_path)

            usrturn_email_subj = usrturn_name_first + ' ' +usrturn_name_last + " has returned from their trip"
            usrturn_email_cont = 'They Returned At ' + str(datetime.now())
            mail_send(usrturn_email_subj,usrturn_email_cont,usrturn_mail_1)

            _=os.system('cls')
            print('User Returned')
            print('1.Exit')
            print('2.Return Another User')
            usr_choice_2 = raw_input('Option:')
            if usr_choice_2 == '1':
                usr_choice = 'a'
            if usr_choice_2 == '2':
                usr_choice_2 = 'a'

    if usr_choice_1 == "1":
        usr_choice_1_1 = 'a'
        while usr_choice_1_1 == "a":
          
            _=os.system("cls")
            print('Please Input Your Details')
            
            usrtemp_name_first = raw_input('First Name:')
            usrtemp_name_last = raw_input('Last Name:')
            usrtemp_mail = raw_input('Your Email:')

            if "@" not in usrtemp_mail:
                print('Invalid Email')
                time.sleep(2.5)
                break
            
            usrtemp_mail_1 = raw_input('Emergency Email Contact:')

            if "@" not in usrtemp_mail_1:
                print('Invalid Email')
                time.sleep(2.5)
                break
            
            usrtemp_time_date = raw_input('Latest Possible Return Date:')
            usrtemp_time_time = raw_input('Latest Possible Return Time:')

            _=os.chdir(installDir)
            _=os.chdir('away')
            f= open(usrtemp_name_first + '#' + usrtemp_name_last + '.txt',"w+")
            f.write(usrtemp_name_first)
            f.write("\n")
            f.write(usrtemp_name_last)
            f.write("\n")
            f.write(usrtemp_mail_1)
            f.write("\n")
            f.write(usrtemp_time_date)
            f.write("\n")
            f.write(usrtemp_time_time)
            f.write("\n")
            f.write(usrtemp_mail)
            f.write("\n")
            f.close()
            
            usrtemp_email_subj = 'You Are ' + usrtemp_name_first + ' ' +usrtemp_name_last + "'s Emergency Contatct"
            usrtemp_email_cont = 'They Expect To Return On The ' + usrtemp_time_date + ' at ' + usrtemp_time_time
            mail_send(usrtemp_email_subj,usrtemp_email_cont,usrtemp_mail_1)

            os.chdir(installDir)
            f= open('togenerate.txt',"w+")
            f.write(usrtemp_name_first + '#' + usrtemp_name_last)
            f.close()
            _=os.system("py test.py")
            time.sleep(1)
            os.remove('togenerate.txt')

            _=os.system('cls')
            print('Journey Started')
            print('Printing Details and Barcode')
            print('1.Exit')
            print('2.Add Another Temporary Account')
            
            usr_choice_1_1 = raw_input('option:')
            if usr_choice_1_1 == '1':
                usr_choice = 'a'
            if usr_choice_1_1 == '2':
                usr_choice_1_1 = 'a'

    if usr_choice_1 == "2":
        usr_choice_1_2 = 'a'
        while usr_choice_1_2 == "a":
          
            _=os.system("cls")
            print('Please Input Your Login Details')
            usrlogin_email = raw_input('Email:')
            if "@" not in usrlogin_email:
                print("Invalid Email Address")
                time.sleep(2.5)
                break
            _=os.chdir(installDir)
            _=os.chdir('accounts')
            usrlogin_email_path = usrlogin_email + '.txt' 
            if os.path.isfile(usrlogin_email_path) == False:
              _=os.system("cls")
              print("Login Doesn't Exist")
              print("Please Create New Account")
              time.sleep(2.5)
              break
            
            usrlogin_pass = raw_input('Password:')
            f= open(usrlogin_email + '.txt',"r")
            usrlogin_name_first = f.readline()
            usrlogin_name_last = f.readline()
            usrlogin_mail_1 = f.readline()
            f.readline()
            f.readline()
            f.readline()
            usrlogin_pass_real = f.readline()
            f.close()
            
            usrlogin_name_first = usrlogin_name_first.rstrip('\n')
            usrlogin_name_last = usrlogin_name_last.rstrip('\n')
            usrlogin_pass_real = usrlogin_pass_real.rstrip('\n')
            usrlogin_mail_1 = usrlogin_mail_1.rstrip('\n')
            
            if usrlogin_pass != usrlogin_pass_real:
              _=os.system("cls")
              print("Incorrect Password")
              time.sleep(2.5)
              break
            
            usrlogin_time_date = raw_input('Latest Possible Return Date:')
            usrlogin_time_time = raw_input('Latest Possible Return Time:')
            
            _=os.chdir(installDir)
            _=os.chdir('away')
            f= open(usrlogin_name_first + '#' + usrlogin_name_last + '.txt',"w+")
            f.write(usrlogin_name_first)
            f.write("\n")
            f.write(usrlogin_name_last)
            f.write("\n")
            f.write(usrlogin_mail_1)
            f.write("\n")
            f.write(usrlogin_time_date)
            f.write("\n")
            f.write(usrlogin_time_time)
            f.write("\n")
            f.close()
            
            usrlogin_email_subj = usrlogin_name_first + ' ' +usrlogin_name_last + " Is Now In The Bush"
            usrlogin_email_cont = 'They Expect To Return On The ' + usrlogin_time_date + ' at ' + usrlogin_time_time
            mail_send(usrlogin_email_subj,usrlogin_email_cont,usrlogin_mail_1)
            
            os.chdir(installDir)
            f= open('togenerate.txt',"w+")
            f.write(usrlogin_name_first + '#' + usrlogin_name_last)
            f.close()
            _=os.system("py test.py")
            time.sleep(1)
            os.remove('togenerate.txt')

            _=os.system('cls')
            print('Journey Started')
            print('Printing Details and Barcode')
            print('1.Exit')
            print('2.Log Into Another Account')
            
            usr_choice_1_2 = raw_input('option:')
            if usr_choice_1_2 == '1':
                usr_choice = 'a'
            if usr_choice_1_2 == '2':
                usr_choice_1_2 = 'a'

    if usr_choice_1 == "3":
        usr_choice_1_3 = 'a'
        while usr_choice_1_3 == "a":
          
            _=os.system("cls")
            print('Creating New Account')
            print('Please Input Your Details')
            
            usrmake_name_first = raw_input('First Name:')
            usrmake_name_last = raw_input('Last Name:')
            usrmake_email = raw_input('Your Email:')
            usrmake_pass = raw_input('Password:')
            usrmake_mail_1 = raw_input('Emergency Email Contact:')
            
            if "@" not in usrmake_mail_1:
                usrtemp_mail_1 = ownerMail
            
            _=os.chdir(installDir)
            _=os.chdir('accounts')
            f= open(usrmake_email + '.txt',"w")
            f.write(usrmake_name_first)
            f.write("\n")
            f.write(usrmake_name_last)
            f.write("\n")
            f.write(usrmake_mail_1)
            f.write("\n")
            f.write('0')
            f.write("\n")
            f.write('0')
            f.write("\n")
            f.write(usrmake_email)
            f.write("\n")
            f.write(usrmake_pass)
            f.write("\n")
            f.close()
            
            usrmake_email_subj = 'You Are ' + usrmake_name_first + ' ' +usrmake_name_last + "'s Emergency Contatct"
            usrmake_email_cont = 'Please Note Any Times Contained In Future Emails'
            mail_send(usrmake_email_subj,usrmake_email_cont,usrmake_mail_1)
            
            _=os.system('cls')
            print('Account Created')
            print('Please Login To Start Journey')
            print('1.Main Menu')
            print('2.Make Another Account')
            
            usr_choice_1_3 = raw_input('option:')
            if usr_choice_1_3 == '1':
                usr_choice = 'a'
            if usr_choice_1_3 == '2':
                usr_choice_1_3 = 'a'
        
    usr_choice = 'a'
    usr_choice_1 = 'a'
    
