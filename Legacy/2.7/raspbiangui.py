import time
import os
import RPi.GPIO as GPIO
import smbus
import glob
import shutil
import os.path
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

emailUsername = 'xxx@gmail.com'
emailPassword = 'xxx'

n2d = 0
cpdir = 0
chdir = 0
I2C_ADDR  = 0x3f 
LCD_WIDTH = 16 
LCD_CHR = 1
LCD_CMD = 0
LCD_LINE_1 = 0x80 
LCD_LINE_2 = 0xC0 
LCD_LINE_3 = 0x94 
LCD_LINE_4 = 0xD4
LCD_BACKLIGHT  = 0x08  # On
#LCD_BACKLIGHT = 0x00  # Off
ENABLE = 0b00000100 # Enable bit
E_PULSE = 0.0005
E_DELAY = 0.0005
#bus = smbus.SMBus(0)  # Rev 1 Pi uses 0
bus = smbus.SMBus(1) # Rev 2 Pi uses 1
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def lcd_init():
  lcd_byte(0x33,LCD_CMD) 
  lcd_byte(0x32,LCD_CMD) 
  lcd_byte(0x06,LCD_CMD) 
  lcd_byte(0x0C,LCD_CMD) 
  lcd_byte(0x28,LCD_CMD) 
  lcd_byte(0x01,LCD_CMD) 
  time.sleep(E_DELAY)
def lcd_byte(bits, mode):
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT
  bits_low = mode | ((bits<<4) & 0xF0) | LCD_BACKLIGHT
  bus.write_byte(I2C_ADDR, bits_high)
  lcd_toggle_enable(bits_high)
  bus.write_byte(I2C_ADDR, bits_low)
  lcd_toggle_enable(bits_low)
def lcd_toggle_enable(bits):
  time.sleep(E_DELAY)
  bus.write_byte(I2C_ADDR, (bits | ENABLE))
  time.sleep(E_PULSE)
  bus.write_byte(I2C_ADDR,(bits & ~ENABLE))
  time.sleep(E_DELAY)
def lcd_string(message,line):
  message = message.ljust(LCD_WIDTH," ")
  lcd_byte(line, LCD_CMD)
  for i in range(LCD_WIDTH):
    lcd_byte(ord(message[i]),LCD_CHR)
def mail_send(subject):
  msg = MIMEMultipart()
  msg['From'] = emailUsername
  msg['To'] = emailUsername
  msg['Subject'] = subject
  body = "YOUR MESSAGE HERE"
  msg.attach(MIMEText(body, 'plain'))
  server = smtplib.SMTP('smtp.gmail.com', 587)
  server.starttls()
  server.login(emailUsername, emailPassword)
  text = msg.as_string()
  server.sendmail(emailUsername, emailUsername, text)
  server.quit()

lcd_init()
lcd_string("Starting",LCD_LINE_1)
lcd_string("JBGuy Tech 2018 ",LCD_LINE_2)
time.sleep(2.0)

lcd_string("Scan Code    [~]",LCD_LINE_1)
lcd_string("Help         [ ]",LCD_LINE_2)
down = 0
countdown = 0

while True:
    brk = 0
    input_state17 = GPIO.input(17)
    if input_state17 == False:
        print('down')
        if countdown == 0:
            lcd_string("Scan Code    [ ]",LCD_LINE_1)
            lcd_string("Help         [~]",LCD_LINE_2)
            down = 1
            time.sleep(0.5)
	if countdown == 1:
            lcd_string("Scan Code    [~]",LCD_LINE_1)
            lcd_string("Help         [ ]",LCD_LINE_2)
            down = 0
            time.sleep(0.5)
	countdown = down
    input_state18 = GPIO.input(18)
    if input_state18 == False:
        print('in')
        if down == 0:
	    input_state17 = GPIO.input(17)
	    while input_state17 == True:
	        print('Scan Code')
                lcd_string("Waiting",LCD_LINE_1)
                lcd_string("Scan Barcode",LCD_LINE_2)
	        barin = raw_input("Barcode: ")
		if "#" not in barin:
		    lcd_string("Invalid",LCD_LINE_1)
		    lcd_string("Barcode",LCD_LINE_2)
		    time.sleep(3.0)
		    lcd_string("Scan Code    [~]",LCD_LINE_1)
		    lcd_string("Help         [ ]",LCD_LINE_2)
		    break
		if barin == "":
		    break
	        mailmsg = barin
	        mail_send(mailmsg+"#s1#nor")
	        barsp = barin.split('#')
	        lcd_string(barsp[0],LCD_LINE_1)
                lcd_string(barsp[1],LCD_LINE_2)
	        t_end = time.time() + 3
	        while time.time() < t_end:
	            input_state17 = GPIO.input(17) 	    
		    if input_state17 == False:
		        break
        if down == 1:       
	    input_state17 = GPIO.input(17)
	    while input_state17 == True:
	        print('Scan Code')
                lcd_string("Waiting",LCD_LINE_1)
                lcd_string("Scan Barcode",LCD_LINE_2)
	        barin = raw_input("Barcode: ")
		if "#" not in barin:
		    lcd_string("Invalid",LCD_LINE_1)
		    lcd_string("Barcode",LCD_LINE_2)
		    time.sleep(3.0)
		    lcd_string("Scan Code    [~]",LCD_LINE_1)
		    lcd_string("Help         [ ]",LCD_LINE_2)
		    break
		if barin == "":
		    break
	        mailmsg = barin
		mail_send(mailmsg+"#s1#sos")
		lcd_string("SOS Help On Way ",LCD_LINE_1)
		lcd_string("Fine if false + ",LCD_LINE_2)
	        barsp = barin.split('#')
	        lcd_string(barsp[0],LCD_LINE_1)
                lcd_string(barsp[1],LCD_LINE_2)
	        t_end = time.time() + 3
	        while time.time() < t_end:
	            input_state17 = GPIO.input(17) 	    
		    if input_state17 == False:
		        break
