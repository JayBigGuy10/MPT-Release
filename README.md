I recomend not installing this project in its current state, I have plans to recreate with arduino and LoRa

# Read This To Install MPT onto your System

# Credit JayBigGuy10 2018 - 2020

## requirements:

- python 3.7 
- python-barcode 0.9
- GMail Account
- MySQL Python Connector

### Optional:

- Pillow 5.2.0 

### Installers:

https://pypi.org/project/python-barcode/ 
use the tarball and admin command prompt and 3.7

https://pypi.org/project/Pillow/ 
pip install pillow

http://dev.mysql.com/downloads/connector/python/
download for your trail start machine platform

To Setup Gmail Go To:
Settings -> Forwarding and POP/IMAP
- Enable POP for All Mail
- When Messages are accessed with POP Keep GMails Copy
- Enable IMAP
- Auto-Expunge Off
- Immediately Delete The Message Forever
- Do Not Limit the Number of Messages
- Allow Less secure apps: ON [Here](https://myaccount.google.com/lesssecureapps?pli=1)

## Configuring: Passwords And Location

windowsgui.py, line 12 'example@gmail.com', line 13 'password', line 14 'C:\Users\User\documents\mpt', line 15 'youremail@email.com'

windowsserver.py, line 14 & 39 'example@gmail.com', line 15 'password', line 16 'C:\Users\User\documents\mpt', line 131 'C:\\\Users\\\user\\\documents\\\mpt\\\overdue\\\\'

raspbiangui.py, line 12 'example@gmail.com', line 13 'password'

## To Do:

- [ ] Make Installer
- [ ] Print Barcodes
- [-] Simplify Credential and Location Changing
- [ ] PIP Installer
- [ ] Website