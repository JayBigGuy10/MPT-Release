import getpass, imaplib
from datetime import datetime
import smtplib
import email
import email.header
import datetime
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import glob
import shutil
import sys
import mysql.connector
from sysconfig import *
import time

_=os.system('title Server')
currentDT = datetime.datetime.now()

def mail_send(subject,content,emailaddr):
  msg = MIMEMultipart()
  msg['From'] = emailUsername
  msg['To'] = emailaddr
  msg['Subject'] = subject
  body = content
  msg.attach(MIMEText(body, 'plain'))
  server = smtplib.SMTP('smtp.gmail.com', 587)
  server.starttls()
  server.login(emailUsername, emailPassword)
  text = msg.as_string()
  server.sendmail(emailUsername, emailaddr, text)
  server.quit()

def mail_delete():
    m = imaplib.IMAP4_SSL('imap.gmail.com', 993)
    m.login(emailUsername,emailPassword)
    m.list()
    m.select('INBOX')
    typ, data = m.search(None, '(FROM "bushtrackernz@gmail.com")')
    ids = data[0]
    ids_list = ids.split()
    for msg in ids_list:
        m.store(msg, '+FLAGS', '\\Deleted')
    m.expunge()
    m.close()
    m.logout()
def mail_read():
    M = imaplib.IMAP4_SSL("imap.gmail.com", 993)
    M.login(emailUsername, emailPassword)
    M.select('inbox')
    typ, data = M.search(None, 'ALL')
    mail_ids = data[0]
    id_list = mail_ids.split()   
    first_email_id = int(id_list[0])
    latest_email_id = int(id_list[-1])
    M.fetch(str(latest_email_id), '(RFC822)' )
    typ, data = M.fetch(str(latest_email_id), '(RFC822)' )
    _=os.system('cls')
    for response_part in data:
        if isinstance(response_part, tuple):
            msg = email.message_from_string(str(response_part[1]))
            email_subject = msg['subject']
            email_from = msg['from']
            #print 'From : ' + email_from
            print ('Subject : ' + str(email_subject))
            return email_subject

def dbq(execute):
    cnx = mysql.connector.connect(user=dbu, password=dbp, host=dbh, database=dbd)
    cursor = cnx.cursor()
    cursor.execute(execute)
    result = cursor.fetchall()
    return(result)
    cnx.close()

def dbe(execute):
    cnx = mysql.connector.connect(user=dbu, password=dbp, host=dbh, database=dbd)
    cursor = cnx.cursor()
    cursor.execute(execute)
    cnx.commit()
    cursor.close()
    cnx.close()
        
def mail_read3():
    M = imaplib.IMAP4_SSL('imap.gmail.com')
    rv, data = M.login(emailUsername, emailPassword)
    rv, mailboxes = M.list()
    rv, data = M.select('INBOX')
    rv, data = M.search(None, "ALL")
    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        msg = email.message_from_bytes(data[0][1])
        hdr = email.header.make_header(email.header.decode_header(msg['Subject']))
        subject = str(hdr)
        return(subject)
        print(subject)

while True:
    ontrail = dbq("select * from ontrail")
    num = int('0')
    peopleAway = True
    if not ontrail:
        _=os.system('cls')
        print('Nobody On Trail')
        peopleAway = False
    else:
        _=os.system('cls')
        print('People On Trails Status Normal')
    if peopleAway == True:
        ontrailover = dbq("select * from ontrail where ReturnDateTime <= '"+ currentDT.strftime("%Y-%m-%d %H:%M:%S") +"' and Overdue = False")
        j = int(0)
        for i in ontrailover:
            person = dbq("select * from accounts where Id = '" + str(ontrailover[j][0]) + "'")
            print("Person Overdue")
            print (person[0][1]+person[0][2])
            dbe("update ontrail set Overdue = True where UserId = " + str(ontrailover[j][0]))
            usr_miss_subj = person[0][1] + " " + person[0][2] + " has not returned from his/her trip!"
            usr_miss_cont = "The expected return was " + str(ontrailover[j][2]) + ", it is now past this date and time.\nPlease try to make contact or alert authorities."
            mail_send(usr_miss_subj,usr_miss_cont,person[0][4])
            num = int('1')
            j = j+1

    sos = True
    AssitNeed = dbq("select * from ontrail where AssitNeed = True")
    if not AssitNeed:
        sos = False

    if sos == True:
        j = int(0)
        for i in sos:
            usr_sos = dbq("select * from accounts where Id = " + str(AssitNeed[j][0]))
            dbe("update ontrail set AssitNeed = False where UserId = " + str(AssitNeed[j][0]))
            usr_sos_subj = usr_sos[0][1] + " " + usr_sos[0][2] + " has raised an SOS!"
            usr_sos_cont = "They are at station " + str(AssitNeed[j][5]) + " in need of assistance!\nPlease try to contact them or alert authorities"
            mail_send(usr_sos_subj,usr_sos_cont,usr_sos[0][4])
            print('sos')
            print(usr_sos_subj)
            print(usr_miss_cont)

    time.sleep(2.0)