import getpass, imaplib
import time
from datetime import datetime
import smtplib
import email
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import barcode
import os
import mysql.connector
from sysconfig import *

_=os.system('title UserGUI')

def mail_send(subject,content,emailaddr):
  msg = MIMEMultipart()
  msg['From'] = emailUsername
  msg['To'] = emailaddr
  msg['Subject'] = subject
  body = content
  msg.attach(MIMEText(body, 'plain'))
  server = smtplib.SMTP('smtp.gmail.com', 587)
  server.starttls()
  server.login(emailUsername, emailPassword)
  text = msg.as_string()
  server.sendmail(emailUsername, emailaddr, text)
  server.quit()

def gen_barcode(barcodevar):
    barcode.PROVIDED_BARCODES
    EAN = barcode.get_barcode_class('code128')
    ean = EAN(barcodevar)
    fullname = ean.save('svg-code128')
    #from barcode.writer import ImageWriter
    #ean = EAN(u'12345678901', writer=ImageWriter())
    #fullname = ean.save('ean13_barcode')
    # u'Jayden#LitolffA'

def dbq(execute):
    cnx = mysql.connector.connect(user=dbu, password=dbp, host=dbh, database=dbd)
    cursor = cnx.cursor()
    cursor.execute(execute)
    result = cursor.fetchall()
    return(result)
    cnx.close()

def dbe(execute):
    cnx = mysql.connector.connect(user=dbu, password=dbp, host=dbh, database=dbd)
    cursor = cnx.cursor()
    cursor.execute(execute)
    cnx.commit()
    cursor.close()
    cnx.close()
    
usr_choice = "a"
usr_choice_1 = "a"

#M = imaplib.IMAP4_SSL("imap.gmail.com", 993)
#M.login(emailUsername, emailPassword)
#M.select('inbox')
#typ, data = M.search(None, 'ALL')
#mail_ids = data[0]
#id_list = mail_ids.split()   
#first_email_id = int(id_list[0])
#latest_email_id = int(id_list[-1])
#M.fetch(latest_email_id, '(RFC822)' )

#typ, data = M.fetch(latest_email_id, '(RFC822)' )
#for response_part in data:
#    if isinstance(response_part, tuple):
#        msg = email.message_from_string(response_part[1])
#        email_subject = msg['subject']
#        email_from = msg['from']
#        print 'From : ' + email_from
#        print 'Subject : ' + email_subject

while True:
    while usr_choice == "a":
        _=os.system("cls")
        print('JBGuy Tech 2018 All Rights Reserved')
        print('Welcome to the NZ Bush Tracker, The date/time is ' + str(datetime.now()))
        print('Options:')
        print('1.Start Trail')
        print('2.Check Out')
        usr_choice = input('Option:')
        if usr_choice != '1' and usr_choice != '2':
            usr_choice = "a"
        time.sleep(0.5)


    if usr_choice == "1": #sign in menu
        usr_choice_1 = "a"
        while usr_choice_1 == "a":
            _=os.system("cls")
            print("Please Choose Check In Option")
            print('1.Sign into existing account')
            print('2.Create New Account')
            print('3.Back')
            usr_choice_1 = input('Option:')
            if usr_choice_1 != '1' and usr_choice_1 != '2' and usr_choice_1 != '3':
                usr_choice_1 = "a"
            elif usr_choice_1 != 'a':
                break

    if usr_choice == "2": #check out menu
        usr_choice_2 = 'a'
        while usr_choice_2 == 'a':
            _=os.system("cls")
            print('Please Scan Barcode')
            barin = input('Here:')
            if "#" not in barin:
                _=os.system("cls")
                print('Invalid Barcode')
                print('Please Scan A Valid Barcode')
                time.sleep(2.5)
                break
            if barin == '':
                break

            userAway = dbq("select * from ontrail where UserBar='"+ barin +"'")
            
            if not userAway:
              _=os.system("cls")
              print("User Not On Trail")
              time.sleep(2.5)
              break
            else:
                dbe("delete ontrail from ontrail where UserBar='"+ barin +"'")

            userInfo = dbq("select * from accounts where Id='"+ userAway[0][0] +"'")
            usrturn_email_subj = userInfo[0][1] + ' ' + userInfo[0][2] + " has returned from their trip"
            usrturn_email_cont = 'They Returned At ' + str(datetime.now())
            mail_send(usrturn_email_subj,usrturn_email_cont,userInfo[0][4])

            _=os.system('cls')
            print('User Returned')
            print('1.Exit')
            print('2.Return Another User')
            usr_choice_2 = input('Option:')
            if usr_choice_2 == '1':
                usr_choice = 'a'
            if usr_choice_2 == '2':
                usr_choice_2 = 'a'

            #usrtemp_email_subj = 'You Are ' + usrtemp_name_first + ' ' +usrtemp_name_last + "'s Emergency Contatct"
            #usrtemp_email_cont = 'They Expect To Return On The ' + usrtemp_time_date + ' at ' + usrtemp_time_time
            #mail_send(usrtemp_email_subj,usrtemp_email_cont,usrtemp_mail_1)

    if usr_choice_1 == "1": # login and start journey
        usr_choice_1_1 = 'a'
        while usr_choice_1_1 == "a":
          
            _=os.system("cls")
            print('Please Input Your Login Details')
            usrlogin_email = input('Email:')
            if "@" not in usrlogin_email:
                print("Invalid Email Address")
                time.sleep(2.5)
                break

            uli = dbq("select * from accounts where SelfEmail='"+ usrlogin_email +"'")

            if not uli:
              _=os.system("cls")
              print("Login Doesn't Exist")
              print("Please Create New Account")
              time.sleep(2.5)
              break
            
            usrlogin_pass = input('Password:')
            
            if usrlogin_pass != uli[0][5]:
              _=os.system("cls")
              print("Incorrect Password")
              time.sleep(2.5)
              break
            
            usrlogin_date_time = input('Latest Possible Return Date (yyyy-mm-dd hh:mm:ss):')

            os.chdir(installDir)
            userBar = uli[0][1] + '#' + uli[0][2]
            gen_barcode(userBar)

            dbe("INSERT INTO ontrail(UserId,UserBar,ReturnDateTime,Overdue,AssitNeed) values (3,'" +userBar+ "','" +usrlogin_date_time+ "',False,False)")
            
            usrlogin_email_subj = uli[0][1] + ' ' + uli[0][2] + " Is Now In The Bush"
            usrlogin_email_cont = 'They Expect To Return On The ' + usrlogin_time_date + ' at ' + usrlogin_time_time
            mail_send(usrlogin_email_subj,usrlogin_email_cont,uli[0][4])
                        
            _=os.system('cls')
            print('Journey Started')
            print('Printing Details and Barcode')
            print('1.Exit')
            print('2.Log Into Another Account')
            
            usr_choice_1_1 = input('option:')
            if usr_choice_1_1 == '1':
                usr_choice = 'a'
            if usr_choice_1_1 == '2':
                usr_choice_1_1 = 'a'

    if usr_choice_1 == "2": #still needs mail send fix
        usr_choice_1_2 = 'a'
        while usr_choice_1_2 == "a":
          
            _=os.system("cls")
            print('Creating New Account')
            print('Please Input Your Details')
            
            usrmake_name_first = input('First Name:')
            usrmake_name_last = input('Last Name:')
            usrmake_age = input('Age:')
            usrmake_email = input('Your Email:')
            usrmake_pass = input('Password:')
            usrmake_mail_1 = input('Emergency Email Contact:')
            
            if "@" not in usrmake_mail_1:
                usrtemp_mail_1 = ownerMail

            dbe("INSERT INTO accounts(FirstName,LastName,SelfEmail,ContactEmail,UserPassword,SelfAge)VALUES ('"+ usrmake_name_first +"','"+ usrmake_name_last +"','"+ usrmake_email +"','"+ usrmake_mail_1 +"','"+ usrmake_pass +"',"+ usrmake_age +")")
            
            usrmake_email_subj = 'You Are ' + usrmake_name_first + ' ' +usrmake_name_last + "'s Emergency Contatct"
            usrmake_email_cont = 'Please Note Any Times Contained In Future Emails'
            mail_send(usrmake_email_subj,usrmake_email_cont,usrmake_mail_1)
            
            _=os.system('cls')
            print('Account Created')
            print('Please Login To Start Journey')
            print('1.Main Menu')
            print('2.Make Another Account')
            
            usr_choice_1_2 = input('option:')
            if usr_choice_1_2 == '1':
                usr_choice = 'a'
            if usr_choice_1_2 == '2':
                usr_choice_1_2 = 'a'
        
    usr_choice = 'a'
    usr_choice_1 = 'a'
    
